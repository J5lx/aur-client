# PHP AUR Client

This is a library for querying the AUR. At the moment it is still very basic
and provides little abstraction, though.

## Usage

```php
<?php

use J5lx\AurClient\AurClient;

$client = new AurClient();

// search for packages containing the keyword "foo"
$client->search("foo");
// same as above, but sort ascending
$client->search("foo", "name");

// search by maintainer
$client->msearch("J5lx");

// receive information on one or more packages
$client->info(["libao-jack", "lib32-libao-jack"]);

// suggest packages (typeahead)
$client->suggest("php7");
//suggest pkgbases
$client->suggest("php7", true);
```

If an error occurs on the server or in the library, the library throws a
`RuntimeException`, but in case there are network errors you should also check
for [Guzzle exceptions](http://docs.guzzlephp.org/en/latest/quickstart.html#exceptions).
