<?php

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

namespace spec\J5lx\AurClient;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use PhpSpec\Exception\Example\FailureException;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;

class AurClientSpec extends ObjectBehavior
{
    protected $searchResults = [
        [
            "ID" => 0,
             "Name" => "foo",
             "PackageBaseID" => 0,
             "PackageBase" => "foo",
             "Version" => "1-1",
             "Description" => "hello",
             "URL" => "http://example.org/foo",
             "NumVotes" => 5,
             "OutOfDate" => null,
             "Maintainer" => "J5lx",
             "FirstSubmitted" => 1,
             "LastModified" => 1,
             "License" => "GPLv3",
             "URLPath" => "/cgit/aur.git/snapshot/foo.tar.gz",
             "CategoryID" => 1
        ],
        [
            "ID" => 1,
             "Name" => "bar",
             "PackageBaseID" => 1,
             "PackageBase" => "bar",
             "Version" => "1-1",
             "Description" => "world",
             "URL" => "http://example.org/bar",
             "NumVotes" => 10,
             "OutOfDate" => null,
             "Maintainer" => "J5lx",
             "FirstSubmitted" => 1,
             "LastModified" => 1,
             "License" => "GPLv3",
             "URLPath" => "/cgit/aur.git/snapshot/bar.tar.gz",
             "CategoryID" => 1
        ]
    ];

    public function getMatchers()
    {
        return [
            'setValue' => function ($subject, $expected, $value) {
                if ($expected !== $value) {
                    throw new FailureException(
                        "expected " . var_export($expected, true) . ", but got " . var_export($value, true)
                    );
                }
                return true;
            }
        ];
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType('J5lx\AurClient\AurClient');
    }

    public function it_uses_custom_client_options()
    {
        $handlerCalled = false;
        $handler = function ($request, $options) use (&$handlerCalled) {
            $handlerCalled = true;
            return new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'search',
                'resultcount' => 2,
                'results' => $this->searchResults
            ]));
        };
        $this->beConstructedWith(['handler' => $handler]);

        $this->search("foo")->shouldSetValue(true, $handlerCalled);
    }

    public function it_sends_search_requests()
    {
        $history = [];
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'search',
                'resultcount' => 2,
                'results' => $this->searchResults
            ])),
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'search',
                'resultcount' => 2,
                'results' => $this->searchResults
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $stack->push(Middleware::history($history));
        $this->beConstructedWith(['handler' => $stack]);

        $this->search("foo")->shouldSetValue(
            "/rpc.php?type=search&arg=foo",
            $history[0]['request']->getRequestTarget()
        );

        $history = [];
        $this->search("foo", "name")->shouldSetValue(
            "/rpc.php?type=search&arg=foo&search_by=name",
            $history[0]['request']->getRequestTarget()
        );
    }

    public function it_understands_search_responses()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'search',
                'resultcount' => 2,
                'results' => $this->searchResults
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $this->beConstructedWith(['handler' => $stack]);

        $this->search("foo")->shouldReturn($this->searchResults);
    }

    public function it_throws_an_exception_if_an_error_occurs()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'error',
                'resultcount' => 0,
                'results' => "Query arg to small"
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $this->beConstructedWith(['handler' => $stack]);

        $this->shouldThrow(new \RuntimeException("Server returned error: Query arg to small"))->duringSearch("foo");
    }

    public function it_throws_an_exception_if_the_response_type_is_unexpected()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'foo',
                'resultcount' => 0,
                'results' => []
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $this->beConstructedWith(['handler' => $stack]);

        $this->shouldThrow(new \RuntimeException("Server returned unexpected response type: foo"))->duringSearch("foo");
    }

    public function it_sends_info_requests()
    {
        $history = [];
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'info',
                'resultcount' => 1,
                'results' => [$this->searchResults[0]]
            ])),
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'info',
                'resultcount' => 1,
                'results' => [$this->searchResults[0]]
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $stack->push(Middleware::history($history));
        $this->beConstructedWith(['handler' => $stack]);

        $this->info(["foo"])->shouldSetValue(
            "/rpc.php?type=info&arg=foo",
            $history[0]['request']->getRequestTarget()
        );

        $history = [];
        $this->info(["foo", "bar"])->shouldSetValue(
            "/rpc.php?type=multiinfo&arg%5B0%5D=foo&arg%5B1%5D=bar",
            $history[0]['request']->getRequestTarget()
        );
    }

    public function it_understands_info_responses()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'info',
                'resultcount' => 1,
                'results' => [$this->searchResults[0]]
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $this->beConstructedWith(['handler' => $stack]);

        $this->info(["foo"])->shouldReturn([$this->searchResults[0]]);
    }

    public function it_understands_multiinfo_responses()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'version' => 1,
                'type' => 'multiinfo',
                'resultcount' => 1,
                'results' => $this->searchResults
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $this->beConstructedWith(['handler' => $stack]);

        $this->info(["foo"])->shouldReturn($this->searchResults);
    }

    public function it_sends_msearch_requests()
    {
        $history = [];
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'type' => 'msearch',
                'results' => $this->searchResults
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $stack->push(Middleware::history($history));
        $this->beConstructedWith(['handler' => $stack]);

        $this->msearch("foo")->shouldSetValue(
            "/rpc.php?type=msearch&arg=foo",
            $history[0]['request']->getRequestTarget()
        );
    }

    public function it_understands_msearch_responses()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode([
                'type' => 'msearch',
                'results' => $this->searchResults
            ]))
        ]);
        $stack = HandlerStack::create($mock);
        $this->beConstructedWith(['handler' => $stack]);

        $this->msearch("foo")->shouldReturn($this->searchResults);
    }

    public function it_sends_suggest_requests()
    {
        $history = [];
        $mock = new MockHandler([
            new Response(200, [], json_encode(['foo', 'foobar'])),
            new Response(200, [], json_encode(['foo', 'foobar']))
        ]);
        $stack = HandlerStack::create($mock);
        $stack->push(Middleware::history($history));
        $this->beConstructedWith(['handler' => $stack]);

        $this->suggest("foo")->shouldSetValue(
            "/rpc.php?type=suggest&arg=foo",
            $history[0]['request']->getRequestTarget()
        );

        $history = [];
        $this->suggest("foo", true)->shouldSetValue(
            "/rpc.php?type=suggest-pkgbase&arg=foo",
            $history[0]['request']->getRequestTarget()
        );
    }

    public function it_returns_suggest_responses()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode(['foo', 'foobar'])),
            new Response(200, [], json_encode(['foo', 'foobar']))
        ]);
        $stack = HandlerStack::create($mock);
        $this->beConstructedWith(['handler' => $stack]);

        $this->suggest("foo")->shouldReturn(['foo', 'foobar']);
        $this->suggest("foo", true)->shouldReturn(['foo', 'foobar']);
    }
}
