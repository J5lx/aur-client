<?php

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

namespace J5lx\AurClient;

use GuzzleHttp\Client;
use RuntimeException;

class AurClient
{
    protected $client = null;

    public function __construct(Array $clientOptions = [])
    {
        $this->client = new Client(array_merge([
            'base_uri' => 'https://aur.archlinux.org/'
        ], $clientOptions));
    }

    protected function doRequest(array $query, array $expectedResponseTypes = []): Array
    {
        $response = $this->client->get('rpc.php', ['query' => $query]);
        $result = json_decode($response->getBody(), true);
        if (isset($result['type'])) {
            if ($result['type'] == "error") {
                throw new RuntimeException("Server returned error: " . $result['results']);
            }
            if (!empty($expectedResponseTypes) && !in_array($result['type'], $expectedResponseTypes)) {
                throw new RuntimeException("Server returned unexpected response type: " . $result['type']);
            }
        }
        return $result;
    }

    public function search(string $keywordString, string $by = ''): Array
    {
        $query = [
            'type' => 'search',
            'arg' => $keywordString
        ];
        if (!empty($by)) {
            $query['search_by'] = $by;
        }
        return $this->doRequest($query, ['search'])['results'];
    }

    public function info(array $packages): Array
    {
        if (count($packages) == 0) {
            return [];
        }
        if (count($packages) == 1) {
            return [$this->doRequest([
                'type' => 'info',
                'arg' => implode($packages)
            ], ['info', 'multiinfo'])['results']];
        } else {
            return $this->doRequest([
                'type' => 'multiinfo',
                'arg' => $packages
            ], ['info', 'multiinfo'])['results'];
        }
    }

    public function msearch(string $maintainer): Array
    {
        return $this->doRequest(['type' => 'msearch', 'arg' => $maintainer], ['msearch', 'search'])['results'];
    }

    public function suggest(string $search, bool $pkgbase = false): Array
    {
        $type = 'suggest';
        if ($pkgbase) {
            $type = 'suggest-pkgbase';
        }
        return $this->doRequest(['type' => $type, 'arg' => $search]);
    }
}
